let env = process.env.NODE_ENV || 'development'
if (env !== 'production') {
  require('dotenv').config()
}

const app = require('express')()

require('./server/config/database')()
require('./server/config/express')(app)
require('./server/config/routes')(app)
require('./server/config/passport')()

let port = process.env.PORT

app.listen(port, () => console.log(`Server listening on port ${port}...`))
