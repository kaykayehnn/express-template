const home = require('./home-controller')
const users = require('./users-controller')
const flights = require('./flights-controller')

module.exports = {
  home,
  users,
  flights
}
